package automation.mobile.desafio_idwall.test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import automation.mobile.desafio_idwall.properties.LoadProperties;
import automation.mobile.desafio_idwall.screen.DogScreen;
import automation.mobile.desafio_idwall.screen.LoginScreen;
import automation.mobile.desafio_idwall.screen.factory.DriverFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseTest{

	protected String userEmail;
	protected DogScreen dogScreen;
	private static AppiumDriver<MobileElement> driver;

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "platform", "device" })
	public void setup(String platform, String device) {
		driver = DriverFactory.getInstance(platform, device);
		
		LoginScreen loginScreen = new LoginScreen(driver);
		loginScreen.login(LoadProperties.getProperty("email"));
		dogScreen = new DogScreen(getDriver());
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}
	
	protected static AppiumDriver<MobileElement> getDriver() {
		return driver;
	}

}
