package automation.mobile.desafio_idwall.test;

import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

public class ShowDogsTest extends BaseTest{
	
	@Test
	public void shouldShowLabradorDogTest() {
		dogScreen.showLabrador();
		assertTrue(dogScreen.isDogGalleryDisplayed());
	}
	
	@Test
	public void shouldShowHuskyDogTest() {
		dogScreen.showHusky();
		assertTrue(dogScreen.isDogGalleryDisplayed());
	}
	
	@Test
	public void shouldShowHoundDogTest() {
		dogScreen.showHound();
		assertTrue(dogScreen.isDogGalleryDisplayed());
	}
	
	@Test
	public void shouldShowPugDogTest() {
		dogScreen.showPug();
		assertTrue(dogScreen.isDogGalleryDisplayed());
	}
}
