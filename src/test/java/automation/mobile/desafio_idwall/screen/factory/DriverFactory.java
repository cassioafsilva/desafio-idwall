package automation.mobile.desafio_idwall.screen.factory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;
import automation.mobile.desafio_idwall.properties.LoadProperties;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class DriverFactory {

	private static AppiumDriver<MobileElement>	driver;
	private static boolean						isAndroid;

	public static AppiumDriver<MobileElement> getInstance(String platform, String device) {

			isAndroid = "android".equalsIgnoreCase(platform);
			System.setProperty("platform", platform);

			DesiredCapabilities capabilities = new DesiredCapabilities();

			if (isAndroid) {
				capabilities.setCapability(MobileCapabilityType.APP, new File(LoadProperties.getPathApp("android")));
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,MobilePlatform.ANDROID);
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device);
				capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
				capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
				try {
					setDriver(new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), capabilities));
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
			} else {
				Logger.getGlobal().severe("========== Plataform name does not exist!! ==========");
			}
		return getDriver();

	}

	public static AppiumDriver<MobileElement> getDriver() {
		return driver;
	}

	public static void setDriver(AppiumDriver<MobileElement> driver) {
		DriverFactory.driver = driver;
	}
}