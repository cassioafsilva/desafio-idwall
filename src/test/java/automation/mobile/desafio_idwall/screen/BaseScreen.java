package automation.mobile.desafio_idwall.screen;


import java.time.Duration;
import java.util.List;
import java.util.logging.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BaseScreen {

	private static final int POLLING_TIMEOUT = 250;
	private static final int MAX_SECONDS_TIMEOUT = 60;
	protected AppiumDriver<MobileElement> driver;
	private Wait<WebDriver> wait;

	public BaseScreen(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
		this.wait =  new FluentWait<WebDriver>(this.driver).
						withTimeout(Duration.ofSeconds(MAX_SECONDS_TIMEOUT)).
						pollingEvery(Duration.ofMillis(POLLING_TIMEOUT)).
						ignoring(NoSuchElementException.class).
						ignoring(StaleElementReferenceException.class).
						ignoring(IndexOutOfBoundsException.class);

	}


	/**
	 * This method click on element in screen
	 * 
	 * @param element
	 */
	protected void clickOn(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	/**
	 * Wait the element to be visible and send the value parameter for this element
	 * Timeout default is 15s
	 * 
	 * @param element
	 * @param value
	 */
	protected void sendValue(MobileElement element, CharSequence value) {
		wait.until(ExpectedConditions.visibilityOf(element));
		element.clear();
		element.sendKeys(value);
	}

	
	public void waitForElementsVisible(List<WebElement> elements) {
		wait.until(ExpectedConditions.visibilityOfAllElements(elements));		
		
	}


	/**
	 * Check if all elements are visible on screen, in case one of them is not
	 * visible the method will return false
	 * 
	 * @param elements
	 * @return boolean
	 */
	protected boolean isVisible(MobileElement... elements) {
		for (MobileElement element : elements) {
			wait.until(ExpectedConditions.visibilityOf(element));
			if (!element.isDisplayed()) {
				Logger.getGlobal().info("Element not visible: ".concat(element.getId()));
				return false;
			}
		}
		return true;
	}
}
