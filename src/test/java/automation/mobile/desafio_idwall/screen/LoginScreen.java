package automation.mobile.desafio_idwall.screen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginScreen extends BaseScreen {

    public LoginScreen(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(id = "email_text")
    private MobileElement inputEmail;

    @AndroidFindBy(id = "signup_button")
    private MobileElement btnLogin;

    public void login(String user) {
    	sendValue(inputEmail, user);
        clickOn(btnLogin);
    }
}
