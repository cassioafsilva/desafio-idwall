package automation.mobile.desafio_idwall.screen;

import java.util.List;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class DogScreen extends BaseScreen {

    public DogScreen(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//*[@content-desc='Husky']")
    private MobileElement huskyBtn;

    @AndroidFindBy(xpath = "//*[@content-desc='Hound']")
    private MobileElement houndBtn;
    
    @AndroidFindBy(xpath = "//*[@content-desc='Labrador']")
    private MobileElement labradorBtn;
    
    @AndroidFindBy(xpath = "//*[@content-desc='Pug']")
    private MobileElement pugBtn;

    @AndroidFindBy(id = "photo_recycler")
    private MobileElement recyclerDogPhotos;
    
    @AndroidFindBy(id = "dog_image")
    private List<WebElement> dogImages;
    
    
    public void showHusky() {
    	clickOn(huskyBtn);
    }
    
    public void showHound() {
    	clickOn(houndBtn);
    }
    
    public void showLabrador() {
    	clickOn(labradorBtn);
    }
    
    public void showPug() {
    	clickOn(pugBtn);
    }

    public boolean isDogGalleryDisplayed() {
    	waitForElementsVisible(dogImages);
    	return isVisible(recyclerDogPhotos) && dogImages.size() > 0;
    }
    
}
