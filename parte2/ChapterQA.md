# Chapter de QA dentro da empresa
* No início de cada quarter fazer um planejamento do que iremos trabalhar com demandas do chapter no quarter, pode ser alguma inovação, algum débito técnico que precisamos resolver, etc. Todos dos chapter tem a liberdade de sugerir o que quiserem, e o chapter todo realiza uma votação para priorizar as demandas

* O Chapter de QA deve realizar uma sync semanal ou quinzenal (atualmente fazemos semanal) onde NÃO É UM STATUS REPORT e sim um momento de iteração entre todos os QA’s. A dinâmica funciona da seguinte forma:
- Uma semana temos uma apresentação técnica, um dojo, ou algo prático
- E outra semana temos um momento livre para fazermos o que tivermos vontade, desde fazer algo off-site, como tomar um açaí, tomar um suco, ou mesmo fazendo algum tipo de alinhamento necessário entre o time todo.

### Considero importante a participação do QA em todo o processo de desenvolvimento:
 * Desde o nascimento da idéia. O QA deve participar junto do PO e UX em todo o processo de Discovery, entrevista com usuários, levantamento de hipóteses identificação de caminhos alternativos de uma feature.

* O QA não é responsável somente por automatizar testes, essa é uma das muitas tarefas de um QA dentro de um time ágil. Temos o papel de garantir a qualidade, da escrita de uma US bem estruturada e clara, com os critérios de aceite, pareamento com os Dev’s para ajudá-los a mapear os cenários de teste, garantir a qualidade de que o que ta sendo desenvolvido é o que realmente é a expectativa do usuário.

* É super importante que os testes sejam muito bem distribuídos de acordo com a pirâmide de testes, onde temos na base da pirâmide todos os testes unitários, no meio temos os testes de integração e no topo da pirâmide temos os testes automatizados e2e.

* Uma boa abordagem eficiente é a utilização de BDD onde o que QA escreve todos os cenários possíveis utilizando Gherkin, e faz a revisão de tais cenários juntamente com o PO, UX e os DEVs. É super importante que seja de forma colaborativa e em conjunto, lembrando sempre que a QA é uma responsabilidade de todos do time e não somente do QA.

### Esses conceitos devem ser o DNA de um Chapter de QA para que todos estejam sempre bem alinhados de como um QA deve ser dentro de um time. 