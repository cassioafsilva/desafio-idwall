# desafio-idwall
---

Projeto utilizando JAVA e Appium para testes end-to-end do aplicativo ID_DOG.

## Pré-requisitos:

- MacOS ou Linux (a explicação abaixo é basedo no MacOS)
- Java (utilizei v8)
- Appium
- Appium Doctor
- Node
- Android Emulator
- Maven (para utilizar o CLI)


---
## Passo-a-passo para setup do projeto

### 1) Configurar as variáveis de ambiente no arquivo _.bash_profile_:

Abrir arquivo:

```
vim ~/.bash_profile
```

Inlcuir as novas variáveis:

```
export JAVA_HOME=$(/usr/libexec/java_home)
export PATH=$PATH:$JAVA_HOME
export PATH=$PATH:$JAVA_HOME/bin

export ANDROID_HOME=/Users/your_username/Library/Android/sdk
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH=$ANDROID_HOME/tools:$PATH
export PATH=$ANDROID_HOME/lib:$PATH
export PATH=$ANDROID_HOME/emulator:$PATH
export PATH=$ANDROID_HOME/tools/lib:$PATH
```


Após salvar, recarregar o arquivo:

```
source ~/.bash_profile
```

### 2) Checar dependências do Appium

Executar:

```
appium-doctor
```

O resultado dever ser algo como:

```
info AppiumDoctor Appium Doctor v.1.4.3
info AppiumDoctor ### Diagnostic starting ###
info AppiumDoctor  ✔ The Node.js binary was found at: /usr/local/bin/node
info AppiumDoctor  ✔ Node version is 10.10.0
info AppiumDoctor  ✔ Xcode is installed at: /Applications/Xcode.app/Contents/Developer
info AppiumDoctor  ✔ Xcode Command Line Tools are installed.
info AppiumDoctor  ✔ DevToolsSecurity is enabled.
info AppiumDoctor  ✔ The Authorization DB is set up properly.
info AppiumDoctor  ✔ Carthage was found at: /usr/local/bin/carthage
info AppiumDoctor  ✔ HOME is set to: /Users/cassio
info AppiumDoctor  ✔ ANDROID_HOME is set to: /Users/cassio/Library/Android/sdk
info AppiumDoctor  ✔ JAVA_HOME is set to: /Library/Java/JavaVirtualMachines/jdk/Contents/Home
info AppiumDoctor  ✔ adb exists at: /Users/cassio/Library/Android/sdk/platform-tools/adb
info AppiumDoctor  ✔ android exists at: /Users/cassio/Library/Android/sdk/tools/android
info AppiumDoctor  ✔ emulator exists at: /Users/cassio/Library/Android/sdk/tools/emulator
info AppiumDoctor  ✔ Bin directory of $JAVA_HOME is set
info AppiumDoctor ### Diagnostic completed, no fix needed. ###
info AppiumDoctor
info AppiumDoctor Everything looks good, bye!
info AppiumDoctor
```

### 3) Iniciar emulador ANDROID
- É necessário utilizar o Android Studio para criar um emulador atráves do AVD (opção existende dentro da IDE)
- O emulador padrão que utilizei foi o Android é Nexus_6_API_28

### 4) Iniciar servidor do Appium

Executar:

```
appium
```

### 5) Suíte de teste

Existem os arquivo androidSuite.xml, que é o arquivo que definimos quais são as classes de teste que compõe cada suite. Esses arquivos passam 2 parametros para a execução do teste, que são:
- platform: que é referente a plataforma da suite
- device:  que é pra indicar em qual device vai ser executado o teste.


### 6) Executar os testes 

Entrar via linha de comando na raiz do projeto e executar:

```
mvn clean test -DsuiteXmlFile=androidSuite.xml
```            

### 7) Defeitos
- Foi encontrado um bug no login, que está permitindo realizar um login sem um usuário pré-cadastrado, ou seja, consegui realizar o login com um usuário `invalido@email.com.br`

### 8) Sugestão
- Adicionar id's no menu, pois foi necessário utilizar XPATH, o que não é uma boa estratégia de localizar elementos, pois pode causar Flaky Tests
